defmodule Grdev.Repo.Migrations.AddTechnologiesAndDomainTables do
  use Ecto.Migration

  def change do
    create table(:technology_domains) do
      add :name, :string, size: 50
    end

    create table(:programming_languages) do
      add :name, :string, size: 50
    end

    create table(:frameworks) do
      add :name, :string, size: 50
    end

    alter table(:job_advertisements) do
      add :description, :string
    end

    create table(:job_advertisement_languages) do
      add :programming_language_id, references("programming_languages")
      add :job_advertisement_id, references("job_advertisements")
    end

    create table(:job_advertisement_frameworks) do
      add :framework_id, references("frameworks")
      add :job_advertisement_id, references("job_advertisements")
    end
  end
end
