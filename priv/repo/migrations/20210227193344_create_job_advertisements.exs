defmodule Grdev.Repo.Migrations.CreateJobAdvertisements do
  use Ecto.Migration

  def change do
    create table(:job_categories) do
      add :name, :string, size: 50
      timestamps()
    end

    create table(:job_posters) do
      add :name, :string, size: 50
      add :email, :string, size: 70
      timestamps()
    end

    create table(:job_advertisements) do
      add :title, :string, size: 50
      add :upload_date, :date
      add :poster_id, references("job_posters", on_delete: :delete_all)
      add :category_id, references("job_categories")

      timestamps()
    end
  end
end
