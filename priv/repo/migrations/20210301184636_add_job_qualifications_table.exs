defmodule Grdev.Repo.Migrations.AddJobQualificationsTable do
  use Ecto.Migration

  def change do
    create table(:job_qualifications) do
      add :lower_salary, :integer, null: false
      add :upper_salary, :integer
      add :lower_years_of_experience, :integer
      add :upper_years_of_experience, :integer

      add :job_advertisement_id, references(:job_advertisements)
    end
  end
end
