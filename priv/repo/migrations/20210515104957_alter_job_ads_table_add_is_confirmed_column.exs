defmodule Grdev.Repo.Migrations.AlterJobAdsTableAddIsConfirmedColumn do
  use Ecto.Migration

  def change do
    alter table(:job_advertisements) do
      add :is_confirmed, :boolean, default: false
    end
  end
end
