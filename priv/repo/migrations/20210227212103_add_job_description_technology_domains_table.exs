defmodule Grdev.Repo.Migrations.AddJobDescriptionTechnologyDomainsTable do
  use Ecto.Migration

  def change do
    create table(:job_technology_domains) do
      add :job_description, references("job_advertisements")
      add :technology_domain, references("technology_domains")
      timestamps()
    end
  end
end
