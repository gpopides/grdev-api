defmodule Grdev.Repo.Migrations.AlterPosterTableAddCompanyName do
  use Ecto.Migration

  def change do
    create table(:companies) do
      add :name, :string
      add :location, :string
      add :size, :integer
      add :type, :string
    end

    alter table(:job_posters) do
      add :company_id, references(:companies)
    end
  end
end
