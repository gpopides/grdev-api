defmodule Grdev.Repo.Migrations.AlterJobAdsTableAddLocationColumn do
  use Ecto.Migration

  def change do
    alter table(:job_advertisements) do
      add :city, :string
      # comma seperated
      add :benefits, :string
    end
  end
end
