defmodule GrdevWeb.SwaggerDefinitions.Aggregates do
  use PhoenixSwagger

  def definitions() do
    %{
      TechnologyAggregate:
        swagger_schema do
          properties do
            name(:string, "Technology's name")
            count(:integer, "Count")
          end

          example(%{
            name: "Elixir",
            id: 5
          })
        end,
      TechnologyAggregateResponse:
        swagger_schema do
          properties do
            frameworks(array(:TechnologyAggregate), "Frameworks")
            languages(array(:TechnologyAggregate), "Languages")
          end
        end,
      SalaryTuple:
        swagger_schema do
          properties do
            lower(:integer)
            upper(:integer)
          end
        end,
      SalaryAggregateResponse:
        swagger_schema do
          properties do
            average(:integer, 1500, format: :int64)
            maxLowerSalary(:integer, 1500, format: :int64)
            maxUpperSalary(:integer, 1500, format: :int64)
            salaries(array(:SalaryTuple))
          end
        end,
      AdvertisementPerCity:
        swagger_schema do
          properties do
            city(:string)
            count(:integer, 10, format: :int64)
          end
        end,
      AdvertisementsAggregateResponse:
        swagger_schema do
          properties do
            numOfAdvertisements(:integer, 10, format: :int64)
            advertisementsPerCity(array(:AdvertisementPerCity))
          end
        end,
      CategorizedSalaryAggregate:
        swagger_schema do
          properties do
            range(
              Schema.new do
                type(:object)

                properties do
                  lower(:integer)
                  upper(:integer)
                end
              end
            )

            salaries(array(:integer))
          end
        end,
      CategorizedSalaryResponse:
        swagger_schema do
          properties do
            salaries(array(:CategorizedSalaryAggregate))
          end
        end
    }
  end
end
