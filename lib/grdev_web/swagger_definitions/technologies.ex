defmodule GrdevWeb.SwaggerDefinitions.Technology do
  use PhoenixSwagger

  def definitions() do
    %{
      Technology:
        swagger_schema do
          properties do
            name(:string, "Technology's name")
            id(:integer, "Count")
          end
        end,
      IndexLanguagesResponse:
        swagger_schema do
          properties do
            languages(array(:Technology))
          end
        end,
      IndexFrameworksResponse:
        swagger_schema do
          properties do
            frameworks(array(:Technology))
          end
        end,
      IndexTechnologyResponse:
        swagger_schema do
          properties do
            frameworks(array(:Technology))
            languages(array(:Technology))
          end
        end
    }
  end
end
