defmodule GrdevWeb.SwaggerDefinitions.Advertisement do
  use PhoenixSwagger

  def definitions() do
    %{
      IndexAdvertisementError:
        swagger_schema do
          properties do
            error(:string, "The error message")
          end
        end,
      Poster:
        swagger_schema do
          properties do
            name(:string, "The posters message")
            email(:string, "The posters email")
            company(Schema.ref(:Company))
          end
        end,
      Qualifications:
        swagger_schema do
          properties do
            lowerSalary(:integer)
            upperSalary(:integer)
            lowerYearsOfExperience(:integer)
            upperYearsOfExperience(:integer, :nullable)
          end
        end,
      Company:
        swagger_schema do
          properties do
            name(:string, "The company's name")
            location(:string, "The comapny's location")
            type(:string, "The comapny's type")
          end
        end,
      Advertisement:
        swagger_schema do
          properties do
            id(:integer)
            title(:string)
            description(:string)
            city(:string)
            programmingLanguages(array(:Technology))
            frameworks(array(:Technology))
            benefits(array(:string))
            poster(Schema.ref(:Poster))
            qualifications(Schema.ref(:Qualifications))
          end
        end,
      IndexAdvertisementResponse:
        swagger_schema do
          properties do
            advertisements(array(:Advertisement))
          end
        end,
      CreateAdvertisementErrorResponse:
        swagger_schema do
          properties do
            error(:string, "missing advertisement data")
          end
        end,
      CreateAdvertisementRequest:
        swagger_schema do
          properties do
            title(:string)
            description(:string)
            city(:string)
            programmingLanguages(array(:integer), "The ids of the programming languages")
            frameworks(array(:integer), "The ids of the frameworks")
            benefits(:string)
            poster(Schema.ref(:Poster))

            qualifications(
              Schema.new do
                properties do
                  lowerSalary(:integer)
                  upperSalary(:integer, :nullable)
                  lowerYearsOfExperience(:integer)
                  upperYearsOfExperience(:integer, :nullable)
                end
              end
            )
          end
        end
    }
  end
end
