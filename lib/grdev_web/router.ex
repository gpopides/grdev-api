defmodule GrdevWeb.Router do
  use GrdevWeb, :router

  pipeline :api do
    plug CORSPlug
    plug :accepts, ["json"]
  end

  scope "/api", GrdevWeb do
    pipe_through :api
    resources "/job_advertisements", AdvertisementsController

    get "/technologies/languages", TechnologyController, :index_languages
    get "/technologies/frameworks", TechnologyController, :index_frameworks
    get "/technologies", TechnologyController, :index_technologies

    scope "/aggregates" do
      get "/technologies", AggregatesController, :index_technology_stats
      get "/salary", AggregatesController, :index_salary_stats
      get "/advertisements", AggregatesController, :index_advertisement_stats
      get "/categorized_salaries", AggregatesController, :index_salary_ranges
    end
  end

  def swagger_info do
    %{
      info: %{
        version: "1.0",
        title: "Grdev API"
      }
    }
  end

  scope "/swagger" do
    forward "/", PhoenixSwagger.Plug.SwaggerUI,
      otp_app: :grdev,
      swagger_file: "swagger.json"
  end
end
