defmodule GrdevWeb.AggregatesView do
  use GrdevWeb, :view
  alias Grdev.Aggregates.Structs.SalaryAggregate
  alias Grdev.Aggregates.Structs.RangedSalaryAggregate
  alias Grdev.Aggregates.Structs.JobAdvertisementAggregate

  def render("technology_aggregates.json", %{result: {language_results, framework_results}}) do
    %{
      languages: render_many(language_results, __MODULE__, "aggregate_result.json", as: :result),
      frameworks: render_many(framework_results, __MODULE__, "aggregate_result.json", as: :result)
    }
  end

  def render("aggregate_result.json", %{result: result}) do
    %{
      name: result.name,
      count: result.count
    }
  end

  def render("salary_stats.json", %{
        result: %SalaryAggregate{} = result
      }) do
    SalaryAggregate.toJson(result)
  end

  def render("advertisement_stats.json", %{result: stats}) do
    %{total_count: count, ads_per_city: ads_per_city} = stats

    %{
      numOfAdvertisements: count,
      advertisementsPerCity:
        render_many(ads_per_city, __MODULE__, "advertisement_aggregate.json", as: :ad_aggregate)
    }
  end

  def render("salary_ranges.json", %{result: salaries}) do
    %{salaries: render_many(salaries, __MODULE__, "grouped_salary.json", as: :salary)}
  end

  def render("salary_ranges.json", %{error: msg}) do
    %{error: msg}
  end

  def render("advertisement_aggregate.json", %{ad_aggregate: ad}) do
    JobAdvertisementAggregate.toJson(ad)
  end

  def render("grouped_salary.json", %{salary: salary}) do
    RangedSalaryAggregate.toJson(salary)
  end
end
