defmodule GrdevWeb.AdvertisementsView do
  use GrdevWeb, :view

  alias Grdev.JobAdvertisements.Schemas.JobAdvertisement
  alias Grdev.JobAdvertisements.Schemas.Poster

  alias GrdevWeb.TechnologyView

  def render("advertisements.json", %{result: {:ok, advertisements, num_total_advertisements}}) do
    %{
      advertisements:
        render_many(advertisements, __MODULE__, "advertisement.json", as: :advertisement),
      count: num_total_advertisements
    }
  end

  def render("advertisements.json", %{result: {:error, error_msg}}) do
    %{
      error: error_msg
    }
  end

  def render("advertisement.json", %{advertisement: advertisement}),
    do: render_advertisement(advertisement)

  def render("create_advertisement_error.json", _) do
    %{error: "missing advertisement data"}
  end

  @spec render_advertisement(advertisement :: %JobAdvertisement{}) :: map()
  defp render_advertisement(advertisement) do
    %{
      id: advertisement.id,
      title: advertisement.title,
      description: advertisement.description,
      poster: render_poster(advertisement.poster),
      city: advertisement.city,
      frameworks: render_many(advertisement.frameworks, TechnologyView, "framework.json"),
      programmingLanguages:
        render_many(advertisement.programming_languages, TechnologyView, "language.json"),
      qualifications: render_qualifications(advertisement.qualifications),
      benefits: render_benefits(advertisement.benefits)
    }
  end

  @spec render_poster(poster :: %Poster{}) :: map()
  defp render_poster(poster) do
    %{
      name: poster.name,
      email: poster.email,
      company: render_company(poster.company)
    }
  end

  defp render_company(company) do
    %{
      name: company.name,
      location: company.location,
      type: company.type
    }
  end

  defp render_qualifications(qualifications) do
    %{
      "lowerSalary" => qualifications.lower_salary,
      "upperSalary" => qualifications.upper_salary,
      "lowerYearsOfExperience" => qualifications.lower_years_of_experience,
      "upperYearsOfExperience" => qualifications.upper_years_of_experience
    }
  end

  @spec render_benefits(benefits :: binary()) :: list(String.t())
  defp render_benefits(benefits) when benefits == nil, do: []

  defp render_benefits(benefits) do
    String.split(benefits, ",")
  end
end
