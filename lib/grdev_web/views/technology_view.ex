defmodule GrdevWeb.TechnologyView do
  use GrdevWeb, :view

  def render("languages.json", %{languages: languages}) do
    %{
      languages: render_many(languages, __MODULE__, "language.json")
    }
  end

  def render("language.json", %{technology: language}) do
    %{
      name: language.name,
      id: language.id
    }
  end

  def render("frameworks.json", %{frameworks: frameworks}) do
    %{
      frameworks: render_many(frameworks, __MODULE__, "framework.json")
    }
  end

  def render("framework.json", %{technology: framework}) do
    %{
      name: framework.name,
      id: framework.id
    }
  end

  def render("technologies.json", %{languages: languages, frameworks: frameworks}) do
    %{
      languages: render_many(languages, __MODULE__, "language.json"),
      frameworks: render_many(frameworks, __MODULE__, "framework.json")
    }
  end
end
