defmodule GrdevWeb.AdvertisementsController do
  use GrdevWeb, :controller
  use PhoenixSwagger, only: swagger_path

  alias Grdev.JobAdvertisements.Contexts.JobAdvertisement
  alias GrdevWeb.SwaggerDefinitions.Advertisement, as: AdvertisementSwagger

  swagger_path :index do
    get("/api/job_advertisements")

    parameters do
      page(:path, :integer, "The page, used for paging", required: true)

      framework_ids(
        :path,
        PhoenixSwagger.Schema.array(:integer),
        "The ids of the frameworks that are present in the job ad",
        required: false
      )

      language_ids(
        :path,
        PhoenixSwagger.Schema.array(:integer),
        "The ids of the languages that are present in the job ad",
        required: false
      )
    end

    description("List aggregates about Frameworks and Programming Languages")
    response(200, "Success", Schema.ref(:IndexAdvertisementResponse))
    response(400, "Error", Schema.ref(:IndexAdvertisementError))
  end

  def index(conn, params) do
    with {:ok, advertisements, total_pages_count} <-
           JobAdvertisement.list_job_advertisements(params) do
      render(conn, "advertisements.json", %{result: {:ok, advertisements, total_pages_count}})
    else
      {:error, reason} ->
        conn
        |> put_status(400)
        |> put_view(GrdevWeb.AdvertisementsView)
        |> render("advertisements.json", %{result: {:error, reason}})
    end
  end

  swagger_path :show do
    get("/api/job_advertisements/{id}")
    parameter(:id, :path, :integer, "Advertisement id")
    description("Show advertisement")
    response(200, "Success", Schema.ref(:Advertisement))
    response(404, "Not found")
  end

  def show(conn, %{"id" => id}) do
    case JobAdvertisement.get_job_advertisement(id) do
      nil -> conn |> put_status(404) |> put_view(GrdevWeb.ErrorView) |> render("404.json")
      advertisement -> render(conn, "advertisement.json", %{result: {:ok, advertisement}})
    end
  end

  swagger_path :create do
    post("/api/job_advertisements")
    parameter(:id, :body, Schema.ref(:CreateAdvertisementRequest), "body")
    description("Create advertisement")
    response(200, "Success", Schema.ref(:Advertisement))
    response(400, "Error", Schema.ref(:CreateAdvertisementErrorResponse))
  end

  def create(conn, params) when params == %{},
    do: conn |> put_status(400) |> render("create_advertisement_error.json")

  def create(conn, params) do
    with {:ok, advertisement} <- JobAdvertisement.create_job_advertisement(params) do
      render(conn, "advertisement.json", %{advertisement: advertisement})
    else
      {:error, changeset} ->
        conn
        |> put_status(400)
        |> put_view(GrdevWeb.ErrorView)
        |> render("400.json", changeset: changeset)
    end
  end

  def swagger_definitions do
    AdvertisementSwagger.definitions()
  end
end
