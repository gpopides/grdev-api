defmodule GrdevWeb.TechnologyController do
  use GrdevWeb, :controller
  use PhoenixSwagger, only: swagger_path

  alias Grdev.TechnologyDomains.Contexts.TechnologyDomain
  alias GrdevWeb.SwaggerDefinitions.Technology

  swagger_path :index_languages do
    get("/api/technologies/languages")
    description("List programming languages")
    response(200, "Success", Schema.ref(:IndexLanguagesResponse))
  end

  def index_languages(conn, _params) do
    conn
    |> put_view(GrdevWeb.TechnologyView)
    |> render("languages.json", %{languages: TechnologyDomain.list_programming_languages()})
  end

  swagger_path :index_frameworks do
    get("/api/technologies/frameworks")
    description("List frameworks")
    response(200, "Success", Schema.ref(:IndexFrameworksResponse))
  end

  def index_frameworks(conn, _params) do
    conn
    |> put_view(GrdevWeb.TechnologyView)
    |> render("frameworks.json", %{frameworks: TechnologyDomain.list_frameworks()})
  end

  swagger_path :index_technologies do
    get("/api/technologies")
    description("List both frameworks and languages")
    response(200, "Success", Schema.ref(:IndexTechnologyResponse))
  end

  def index_technologies(conn, _) do
    render(conn, "technologies.json", TechnologyDomain.list_technologies())
  end

  def swagger_definitions do
    Technology.definitions()
  end
end
