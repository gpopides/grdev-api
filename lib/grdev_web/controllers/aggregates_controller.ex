defmodule GrdevWeb.AggregatesController do
  use GrdevWeb, :controller
  use PhoenixSwagger, only: swagger_path

  alias Grdev.Aggregates.Contexts.Aggregates
  alias GrdevWeb.SwaggerDefinitions.Aggregates, as: AggegatesSwagger

  swagger_path :index_technology_stats do
    get("/api/aggregates/technologies")
    description("List aggregates about Frameworks and Programming Languages")
    response(200, "Success", Schema.ref(:TechnologyAggregateResponse))
  end

  def index_technology_stats(conn, _params) do
    render(conn, "technology_aggregates.json", %{result: Aggregates.list_technology_aggregates()})
  end

  swagger_path :index_salary_stats do
    get("/api/aggregates/salary")
    description("List statistics about salaries")
    response(200, "Success", Schema.ref(:SalaryAggregateResponse))
  end

  def index_salary_stats(conn, _params) do
    render(conn, "salary_stats.json", %{result: Aggregates.salary_stats()})
  end

  swagger_path :index_advertisement_stats do
    get("/api/aggregates/advertisements")
    description("List statistics about job advertisements")
    response(200, "Success", Schema.ref(:AdvertisementsAggregateResponse))
  end

  swagger_path :index_salary_ranges do
    get("/api/aggregates/categorized_salaries")

    parameter(:category, :query, :string, "salary category",
      required: true,
      example: ["lower", "upper"]
    )

    description("List statistics about salaries in categories (salary ranges)")
    response(200, "Success", :CategorizedSalaryResponse)
    response(400, "Error", %{error: "Salary category is missing"})
  end

  def index_advertisement_stats(conn, _params) do
    render(conn, "advertisement_stats.json", %{result: Aggregates.advertisement_stats()})
  end

  def index_salary_ranges(conn, %{"category" => category}) do
    render(conn, "salary_ranges.json", %{result: Aggregates.range_grouped_salaries(category)})
  end

  def index_salary_ranges(conn, _params) do
    conn
    |> put_status(400)
    |> put_view(GrdevWeb.AggregatesView)
    |> render("salary_ranges.json", %{error: "Salary category is missing"})
  end

  def swagger_definitions do
    AggegatesSwagger.definitions()
  end
end
