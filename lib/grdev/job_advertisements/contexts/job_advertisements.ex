defmodule Grdev.JobAdvertisements.Contexts.JobAdvertisement do
  @moduledoc """
  The JobAdvertisements context.
  """

  @type list_advertisement_params() :: %{
          framework_ids: list(integer()),
          language_ids: list(integer()),
          page: integer()
        }

  import Ecto.Query, warn: false
  alias Grdev.Repo

  alias Grdev.JobAdvertisements.Schemas.JobAdvertisement

  alias Grdev.Caches.AdvertisementCache

  @preloaded_columns [:programming_languages, :frameworks, [poster: :company], :qualifications]
  @results_limit 5

  @doc """
  Returns a list of job_advertisements.

  ## Examples

      iex> list_job_advertisements(%{})
      [%JobAdvertisement{}, ...]
      iex> list_job_advertisements(%{"framework_ids" => [1]})
      [%JobAdvertisement{}, ...]
      iex> list_job_advertisements(%{"languages" => [1]})
      [%JobAdvertisement{}, ...]
      iex> list_job_advertisements(%{"framework_ids" => [1], "languages" => [1]})
      [%JobAdvertisement{}, ...]

  """
  @spec list_job_advertisements(params :: list_advertisement_params()) ::
          {:ok, list(%JobAdvertisement{}), total_pages_count :: integer()} | {:error, String.t()}
  def list_job_advertisements(params) do
    with {:ok, {cached_results, cached_count}} <- AdvertisementCache.find(params) do
      {:ok, cached_results, cached_count}
    else
      {:error, _} ->
        with {:ok, query_results, count} <- list_with_filters(params) do
          AdvertisementCache.insert(params, {query_results, count})
          {:ok, query_results, count}
        else
          {:error, msg} -> {:error, msg}
        end
    end
  end

  @doc """
  Gets a single job_advertisement.

  Raises `Ecto.NoResultsError` if the Job advertisement does not exist.

  ## Examples

      iex> get_job_advertisement(123)
      %JobAdvertisement{}

      iex> get_job_advertisement(456)
      ** (Ecto.NoResultsError)

  """
  def get_job_advertisement(id),
    do: Repo.get(JobAdvertisement, id) |> Repo.preload(@preloaded_columns)

  @doc """
  Creates a job_advertisement.

  ## Examples

      iex> create_job_advertisement(%{field: value})
      {:ok, %JobAdvertisement{}}

      iex> create_job_advertisement(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_job_advertisement(attrs :: map()) ::
          {:ok, %JobAdvertisement{}} | {:error, %Ecto.Changeset{}}
  def create_job_advertisement(attrs \\ %{}) do
    %JobAdvertisement{}
    |> JobAdvertisement.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a job_advertisement.

  ## Examples

      iex> update_job_advertisement(job_advertisement, %{field: new_value})
      {:ok, %JobAdvertisement{}}

      iex> update_job_advertisement(job_advertisement, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_job_advertisement(%JobAdvertisement{} = job_advertisement, attrs) do
    job_advertisement
    |> JobAdvertisement.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a job_advertisement.

  ## Examples

      iex> delete_job_advertisement(job_advertisement)
      {:ok, %JobAdvertisement{}}

      iex> delete_job_advertisement(job_advertisement)
      {:error, %Ecto.Changeset{}}

  """
  def delete_job_advertisement(%JobAdvertisement{} = job_advertisement) do
    Repo.delete(job_advertisement)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking job_advertisement changes.

  ## Examples

      iex> change_job_advertisement(job_advertisement)
      %Ecto.Changeset{source: %JobAdvertisement{}}

  """
  def change_job_advertisement(%JobAdvertisement{} = job_advertisement) do
    JobAdvertisement.changeset(job_advertisement, %{})
  end

  @doc """
  Return the number of stored advertisements
  """
  @spec num_of_advertisements() :: integer()
  def num_of_advertisements() do
    Repo.aggregate(JobAdvertisement, :count, :id)
  end

  @doc """
  Return the ads count of each city
  """
  @spec ads_per_city() :: %{String.t() => integer()}
  def ads_per_city() do
    from(
      ad in JobAdvertisement,
      group_by: :city,
      order_by: :city,
      select: [ad.city, count(ad.city)]
    )
    |> Repo.all()
  end

  defp list_with_filters(params) do
    %{"page" => page} = params

    page = page || 1

    with {:ok, results_offset} <- get_offset(page) do
      advertisements =
        JobAdvertisement
        |> preload(^@preloaded_columns)
        |> load_relation(params, "frameworks")
        |> load_relation(params, "languages")
        |> offset(^results_offset)
        |> where(^filter_where(params))
        |> distinct(:id)
        |> Repo.all()

      q_for_count =
        from(JobAdvertisement,
          preload: ^@preloaded_columns
        )

      {:ok, advertisements, calculate_pages(q_for_count)}
    else
      {:error, reason} -> {:error, reason}
    end
  end

  defp calculate_pages(%Ecto.Query{} = query) do
    num_of_records = Repo.aggregate(query, :count, :id)
    Float.ceil(num_of_records / @results_limit) |> trunc
  end

  defp filter_where(params) do
    Enum.reduce(params, dynamic(true), fn
      {"language_ids", value}, dynamic ->
        dynamic([programming_languages: pl], ^dynamic and pl.id in ^value)

      {"framework_ids", value}, dynamic ->
        dynamic([frameworks: f], ^dynamic and f.id in ^value)

      {_, _}, dynamic ->
        dynamic
    end)
  end

  defp load_relation(query, params, relation) when relation == "frameworks" do
    if Map.has_key?(params, "framework_ids") do
      query |> join(:inner, [ja], assoc(ja, :frameworks), as: :frameworks)
    else
      query
    end
  end

  defp load_relation(query, params, relation) when relation == "languages" do
    if Map.has_key?(params, "language_ids") do
      query |> join(:inner, [ja], assoc(ja, :programming_languages), as: :programming_languages)
    else
      query
    end
  end

  defp get_offset(page) when is_integer(page) and page > 0 do
    {:ok, @results_limit * (page - 1)}
  end

  defp get_offset(page) when is_integer(page), do: {:error, "page can not have a negative value"}

  defp get_offset(page) when is_bitstring(page) do
    try do
      get_offset(String.to_integer(page))
    rescue
      ArgumentError -> {:error, "invalid page"}
    end
  end
end
