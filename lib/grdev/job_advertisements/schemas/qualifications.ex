defmodule Grdev.JobAdvertisements.Schemas.Qualifications do
  use Ecto.Schema
  import Ecto.Changeset

  schema "job_qualifications" do
    field :lower_salary, :integer
    field :upper_salary, :integer
    field :lower_years_of_experience, :integer
    field :upper_years_of_experience, :integer

    belongs_to :job_advertisement, Grdev.JobAdvertisements.Schemas.JobAdvertisement
  end

  @doc false
  def changeset(qualifications, attrs) do
    qualifications
    |> cast(attrs, [
      :lower_salary,
      :upper_salary,
      :upper_years_of_experience,
      :lower_years_of_experience
    ])
    |> validate_required([:lower_salary, :lower_years_of_experience])
    |> validate_inclusion(:lower_salary, 0..100_000, message: "Can't be greater than 100.000")
    |> validate_inclusion(:upper_salary, 0..100_000, message: "Can't be greater than 100.000")
    |> validate_salary
    |> normalize_salaries
  end

  defp validate_salary(changeset) do
    lower_salary = get_field(changeset, :lower_salary)
    upper_salary = get_field(changeset, :upper_salary)
    check_salaries(changeset, lower_salary, upper_salary)
  end

  defp check_salaries(changeset, _lower_salary, nil), do: changeset

  defp check_salaries(changeset, lower_salary, upper_salary) when lower_salary < upper_salary,
    do: changeset

  defp check_salaries(changeset, lower_salary, upper_salary) when lower_salary > upper_salary,
    do: add_error(changeset, :lower_salary, "cant be greater than upper salary")

  # if upper salary is null, just set lower salary as upper salary
  defp normalize_salaries(changeset) do
    lower_salary = get_field(changeset, :lower_salary)
    upper_salary = get_field(changeset, :upper_salary)

    if upper_salary == nil do
      put_change(changeset, :upper_salary, lower_salary)
    else
      changeset
    end
  end
end
