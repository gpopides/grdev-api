defmodule Grdev.JobAdvertisements.Schemas.Company do
  use Ecto.Schema
  import Ecto.Changeset

  alias Grdev.JobAdvertisements.Schemas.Poster

  schema "companies" do
    field :name, :string
    field :location, :string
    field :size, :integer
    field :type, :string

    has_many :employees, Poster, foreign_key: :company_id
  end

  @spec changeset(
          {map, map}
          | %{
              :__struct__ => atom | %{:__changeset__ => map, optional(any) => any},
              optional(atom) => any
            },
          :invalid | %{optional(:__struct__) => none, optional(atom | binary) => any}
        ) :: Ecto.Changeset.t()
  @doc false
  def changeset(company, attrs) do
    company
    |> cast(attrs, [:name, :location, :type, :size])
    |> validate_required([:name, :location, :type])
  end
end
