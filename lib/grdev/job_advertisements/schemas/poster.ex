defmodule Grdev.JobAdvertisements.Schemas.Poster do
  use Ecto.Schema
  import Ecto.Changeset

  alias Grdev.JobAdvertisements.Schemas.JobAdvertisement
  alias Grdev.JobAdvertisements.Schemas.Company

  schema "job_posters" do
    field :name, :string
    field :email, :string
    has_many :advertisements, JobAdvertisement
    belongs_to :company, Company
    timestamps()
  end

  @doc false
  def changeset(poster, attrs) do
    poster
    |> cast(attrs, [:name, :email])
    |> cast_assoc(:company, with: &Company.changeset/2)
    |> validate_required([:name, :email, :company])
  end
end
