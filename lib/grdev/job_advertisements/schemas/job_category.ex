defmodule Grdev.JobAdvertisements.Schemas.JobCategory do
  use Ecto.Schema
  import Ecto.Changeset

  alias Grdev.JobAdvertisements.Schemas.JobAdvertisement

  schema "job_categories" do
    field :name, :string
    has_many :advertisements, JobAdvertisement, foreign_key: :category_id
    timestamps()
  end

  @doc false
  def changeset(category, attrs) do
    category
    |> cast(attrs, [])
    |> validate_required([])
  end
end
