defmodule Grdev.JobAdvertisements.Schemas.JobAdvertisement do
  use Ecto.Schema

  import Ecto.Changeset

  alias Grdev.JobAdvertisements.Schemas.Poster
  alias Grdev.JobAdvertisements.Schemas.Qualifications
  alias Grdev.JobAdvertisements.Schemas.JobCategory

  alias Grdev.TechnologyDomains.Schemas.Framework
  alias Grdev.TechnologyDomains.Schemas.ProgrammingLanguage
  alias Grdev.TechnologyDomains.Schemas.JobAdvertisementLanguage
  alias Grdev.TechnologyDomains.Schemas.JobAdvertisementFramework

  alias Grdev.TechnologyDomains.Contexts.TechnologyDomain

  schema "job_advertisements" do
    field :title, :string
    field :description, :string
    field :upload_date, :date
    field :city, :string
    field :benefits, :string

    belongs_to :poster, Poster
    belongs_to :category, JobCategory

    has_one :qualifications, Qualifications

    many_to_many :frameworks,
                 Framework,
                 join_through: JobAdvertisementFramework,
                 join_keys: [job_advertisement_id: :id, framework_id: :id]

    many_to_many :programming_languages,
                 ProgrammingLanguage,
                 join_through: JobAdvertisementLanguage,
                 join_keys: [job_advertisement_id: :id, programming_language_id: :id]

    timestamps()
  end

  @doc false
  def changeset(job_advertisement, attrs) do
    job_advertisement
    |> cast(attrs, [:title, :description, :city, :benefits])
    |> cast_assoc(:poster, with: &Poster.changeset/2)
    |> cast_assoc(:qualifications, with: &Qualifications.changeset/2)
    |> put_assoc(:frameworks, parse_frameworks(attrs))
    |> put_assoc(:programming_languages, parse_languages(attrs))
    |> validate_required([:title, :description, :poster, :city, :qualifications])
  end

  defp parse_languages(%{programming_languages: languages}), do: _parse_languages(languages)
  defp parse_languages(%{"programming_languages" => languages}), do: _parse_languages(languages)
  defp parse_languages(%{}), do: []

  defp parse_frameworks(%{frameworks: frameworks}), do: _parse_frameworks(frameworks)
  defp parse_frameworks(%{"frameworks" => frameworks}), do: _parse_frameworks(frameworks)
  defp parse_frameworks(%{}), do: []

  defp _parse_frameworks(framework_ids) do
    with {:ok, frameworks} <-
           TechnologyDomain.list_technologies_with_ids(framework_ids, :framework) do
      frameworks
    else
      {:error, _reason} -> []
    end
  end

  defp _parse_languages(language_ids) do
    with {:ok, languages} <- TechnologyDomain.list_technologies_with_ids(language_ids, :language) do
      languages
    else
      {:error, _reason} -> []
    end
  end
end
