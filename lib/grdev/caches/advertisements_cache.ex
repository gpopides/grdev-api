defmodule Grdev.Caches.AdvertisementCache do
  @moduledoc """
  This Gen server is used for caching job advertisements.
  The cache limit is 40 db query results (depending on filters)
  """

  use GenServer
  require Logger
  alias Grdev.Caches.AdvertisementCacheWorker
  alias Grdev.Caches.Utils.AdvertisementCacheUtils

  @cache_name :AdvertisementCache
  @cache_max_entries 40
  @minute_in_ms 10000

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts ++ [name: __MODULE__])
  end

  @impl true
  def init(_) do
    Logger.info("Starting AdvertisementCache")
    init_cache()
    prepare_cleanup(0)
    {:ok, []}
  end

  @spec find(params :: map()) :: {:ok, results :: any()} | {:error, :not_found}
  def find(params) do
    GenServer.call(__MODULE__, {:find, params})
  end

  @spec insert(params :: map(), value :: any()) :: any()
  def insert(params, value) do
    GenServer.cast(__MODULE__, {:insert, params, value})
  end

  @doc """
  reset ets table
  """
  @spec remove(key :: map()) :: any()
  def remove(key) do
    GenServer.cast(__MODULE__, {:delete, key})
  end

  @impl true
  def handle_info({:cleanup, counter}, state) do
    unless counter == 0 do
      Logger.info("Reseting advertisements cache")
      :ets.delete_all_objects(@cache_name)
    end

    prepare_cleanup(counter + 1)
    {:noreply, state}
  end

  @impl true
  def handle_call({:find, params}, _from, state) do
    cache_key = AdvertisementCacheUtils.generate_cache_key(params)

    case AdvertisementCacheUtils.init_worker() |> AdvertisementCacheWorker.find(cache_key) do
      nil -> {:reply, {:error, :not_found}, state}
      result -> {:reply, {:ok, result}, state}
    end
  end

  @impl true
  def handle_cast({:delete, params}, state) do
    cache_key = AdvertisementCacheUtils.generate_cache_key(params)

    if cache_key != nil do
      delete(cache_key)
    end

    {:noreply, state}
  end

  @impl true
  def handle_cast({:insert, params, value}, state) do
    add_to_cache(params, value)
    {:noreply, state}
  end

  defp add_to_cache(params, value) do
    if count_entries() > @cache_max_entries do
      # find the oldest entry, delete it and add the new entry
      {key, _, _} = find_oldest_cache_entry()
      delete(key)
    end

    insert_to_cache(params, value)
  end

  defp count_entries() do
    length(find_all())
  end

  defp insert_to_cache(params, value),
    do: :ets.insert(@cache_name, AdvertisementCacheUtils.create_insert_tuple(params, value))

  defp find_all() do
    :ets.select(:AdvertisementCache, [{{:_, :_, :_}, [], [:"$_"]}])
  end

  defp delete(key) when key != nil, do: :ets.delete(@cache_name, key)

  def find_oldest_cache_entry() do
    find_all()
    |> Enum.sort(fn a, b ->
      {_, timestamp1, _} = a
      {_, timestamp2, _} = b

      timestamp1 < timestamp2
    end)
    |> List.first()
  end

  defp init_cache() do
    with @cache_name <-
           :ets.new(@cache_name, [:named_table, :set, :protected, read_concurrency: true]) do
      Logger.info("AdvertisementCache ETS Table initiated")
    else
      _ -> Logger.error("AdvertisementCache ETS Table failed to initiate")
    end
  end

  defp prepare_cleanup(counter) do
    Process.send_after(self(), {:cleanup, counter}, @minute_in_ms * 30)
  end
end
