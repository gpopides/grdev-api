defmodule Grdev.Caches.AdvertisementCacheWorker do
  use GenServer

  @cache_name :AdvertisementCache

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  @impl true
  def init(_opts) do
    {:ok, []}
  end

  @spec find(pid :: pid(), key :: String.t()) :: any() | nil
  def find(pid, key) do
    GenServer.call(pid, {:find, key})
  end

  @impl true
  def handle_call({:find, key}, _from, state) do
    {:reply, find_by_key(key), state}
  end

  # defp key_exists(key) do
  # length(lookup(key)) > 0
  # end

  @spec find_by_key(key :: String.t() | nil) :: any() | nil
  def find_by_key(nil), do: nil

  def find_by_key(key) do
    case lookup(key) |> List.first() do
      {_key, _timestamp, items} -> items
      _ -> nil
    end
  end

  defp lookup(nil), do: nil

  defp lookup(key) do
    :ets.lookup(@cache_name, key)
  end
end
