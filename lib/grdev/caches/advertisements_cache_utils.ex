defmodule Grdev.Caches.Utils.AdvertisementCacheUtils do
  alias Grdev.Caches.AdvertisementCacheWorker
  alias Grdev.Caches.CacheSupervisor

  def create_insert_tuple(params, value) do
    {generate_cache_key(params), Time.utc_now(), value}
  end

  @spec generate_cache_key(params :: map()) :: String.t() | :error
  def generate_cache_key(%{"page" => page, "framework_ids" => frameworks}) do
    "page_#{page}_framworks_#{Enum.join(frameworks, "-")}"
  end

  def generate_cache_key(%{"page" => page, "language_ids" => languages}) do
    "page_#{page}_languages_#{Enum.join(languages, "-")}"
  end

  def generate_cache_key(%{
        "page" => page,
        "framework_ids" => frameworks,
        "language_ids" => languages
      }) do
    "page_#{page}_framworks_#{Enum.join(frameworks, "-")}_languages_#{Enum.join(languages)}"
  end

  def generate_cache_key(%{"page" => page}), do: "page_#{page}"
  def generate_cache_key(_), do: :error

  def init_worker() do
    {:ok, pid} = CacheSupervisor.start_child(AdvertisementCacheWorker)
    pid
  end
end
