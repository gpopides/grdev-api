alias Grdev.Repo
alias Grdev.TechnologyDomains.Schemas.ProgrammingLanguage
alias Grdev.TechnologyDomains.Schemas.Framework

defmodule Grdev.Seeder do
  def seed() do
    programming_languages =
      [
        "Java",
        "Kotlin",
        "Elixir",
        "Erlang",
        "C",
        "C++",
        "Python",
        "JavaScript",
        "TypeScript",
        "PHP",
        "C#",
        "F#",
        "Ruby",
        "GO"
      ]
      |> Enum.map(fn x -> %{name: x} end)

    frameworks =
      [
        "Spring Boot",
        "Phoenix",
        "React",
        "Angular",
        "VueJS",
        ".NET",
        ".NET Core",
        "Laravel",
        "Symfony",
        "Lumen",
        "Django",
        "Flask",
        "Ruby on Rails"
      ]
      |> Enum.map(fn x -> %{name: x} end)

    Repo.insert_all(ProgrammingLanguage, programming_languages)
    Repo.insert_all(Framework, frameworks)
  end
end
