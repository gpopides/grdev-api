defmodule Grdev.Repo do
  use Ecto.Repo,
    otp_app: :grdev,
    adapter: Ecto.Adapters.Postgres
end
