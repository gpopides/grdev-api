defmodule Grdev.TechnologyDomains.Contexts.TechnologyDomain do
  @moduledoc """
  The TechnologyDomains context.
  """

  import Ecto.Query, warn: false

  alias Grdev.TechnologyDomains.Contexts.Framework
  alias Grdev.TechnologyDomains.Contexts.ProgrammingLanguage

  @type language_demand_pair() :: [String.t() | integer()]
  @type framework_demand_pair() :: [String.t() | integer()]

  @doc """
  Returns the list of programming languages.

  ## Examples

      iex> list_programming_languages()
      [%ProgrammingLanguage{}, ...]

  """
  def list_programming_languages do
    ProgrammingLanguage.list_programming_languages()
  end

  @doc """
  Returns the list of programming languages.

  ## Examples

      iex> list_frameworks()
      [%Framework{}, ...]

  """
  def list_frameworks do
    Framework.list_frameworks()
  end

  @doc """
  Returns the frameworks and programming languages available

  ## Examples

      iex> list_technologies()
      %{
        languages: [%ProgrammingLanguage{}, ...],
        frameworks: [%Framework{}, ...]
      }

  """
  @spec list_technologies() :: %{
          languages: list(%Grdev.TechnologyDomains.Schemas.ProgrammingLanguage{}),
          frameworks: list(%Grdev.TechnologyDomains.Schemas.Framework{})
        }
  def list_technologies() do
    %{
      languages: list_programming_languages(),
      frameworks: list_frameworks()
    }
  end

  @spec list_technologies_with_ids([integer], :language | :framework) ::
          {:ok,
           [
             %Grdev.TechnologyDomains.Schemas.ProgrammingLanguage{
               __meta__: any,
               id: any,
               name: any
             }
           ]}
          | {:ok, [%Grdev.TechnologyDomains.Schemas.Framework{__meta__: any, id: any, name: any}]}
          | {:error, String.t()}
  def list_technologies_with_ids(ids, :language),
    do: {:ok, ProgrammingLanguage.list_languages_with_ids(ids)}

  def list_technologies_with_ids(ids, :framework),
    do: {:ok, Framework.list_frameworks_with_ids(ids)}

  def list_technologies_with_ids(_ids, _), do: {:error, "key not not supported"}

  @spec technologies_per_advertisements() ::
          {list(language_demand_pair()), list(framework_demand_pair())}
  def technologies_per_advertisements() do
    {ProgrammingLanguage.advertisement_languages_demand(),
     Framework.advertisement_frameworks_demand()}
  end
end
