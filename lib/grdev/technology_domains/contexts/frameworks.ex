defmodule Grdev.TechnologyDomains.Contexts.Framework do
  @type ids :: list(integer())

  @moduledoc """
  The Framework context.
  """

  import Ecto.Query, warn: false
  alias Grdev.Repo
  alias Grdev.TechnologyDomains.Schemas.Framework

  @type framework_demand_pair() :: [String.t() | integer()]

  @doc """
  Returns the list of programming languages.

  ## Examples

      iex> list_frameworks()
      [%Framework{}, ...]

  """
  def list_frameworks do
    Framework |> order_by(asc: :name) |> Repo.all()
  end

  @doc """
  Returns the list of languages based on ids.

  ## Examples

      iex> list_frameworks_with_ids([1])
      [%Framework{}, ...]

  """
  @spec list_frameworks_with_ids(ids :: list(integer())) :: list(%Framework{})
  def list_frameworks_with_ids(ids) do
    q = from pl in Framework, where: pl.id in ^ids
    Repo.all(q)
  end

  @doc """
  Return the frameworks with a counter which
  indicates how many job ads require each language

  ## Examples
      iex> advertisement_languages_demand()
      [["Phoenix", 2], ["Spring Boot", 5], ...]
  """
  @spec advertisement_frameworks_demand() :: list(framework_demand_pair())
  def advertisement_frameworks_demand() do
    q =
      from(
        f in Framework,
        left_join: c in assoc(f, :advertisements),
        group_by: [f.id],
        select: [f.name, count(c)],
        order_by: [desc: count(c)]
      )

    Repo.all(q)
  end
end
