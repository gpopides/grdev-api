defmodule Grdev.TechnologyDomains.Contexts.ProgrammingLanguage do
  @moduledoc """
  The ProgrammingLanguage context.
  """

  import Ecto.Query, warn: false
  alias Grdev.Repo

  alias Grdev.TechnologyDomains.Schemas.ProgrammingLanguage

  @type language_demand_pair() :: [String.t() | integer()]

  @doc """
  Returns the list of programming languages.

  ## Examples

      iex> list_programming_languages()
      [%ProgrammingLanguage{}, ...]

  """
  def list_programming_languages do
    ProgrammingLanguage |> order_by(asc: :name) |> Repo.all()
  end

  @doc """
  Returns the list of languages based on ids.

  ## Examples

      iex> list_languages_with_ids()
      [%ProgrammingLanguage{}, ...]

  """
  @spec list_languages_with_ids(ids :: list(integer())) :: list(%ProgrammingLanguage{})
  def list_languages_with_ids(ids) do
    q = from pl in ProgrammingLanguage, where: pl.id in ^ids
    Repo.all(q)
  end

  @doc """
  Return the programming languages with a counter which
  indicates how many job ads require each language

  ## Examples
      iex> advertisement_languages_demand()
      [["Elixir", 2], ["Java", 5], ...]
  """
  @spec advertisement_languages_demand() :: list(language_demand_pair())
  def advertisement_languages_demand() do
    q =
      from(
        p in ProgrammingLanguage,
        left_join: c in assoc(p, :advertisements),
        group_by: [p.name],
        select: [p.name, count(c)],
        order_by: [desc: count(c)]
      )

    Repo.all(q)
  end
end
