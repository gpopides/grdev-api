defmodule Grdev.TechnologyDomains.Schemas.JobAdvertisementFramework do
  use Ecto.Schema

  @primary_key false
  schema "job_advertisement_frameworks" do
    belongs_to :job_advertisement, Grdev.JobAdvertisements.Schemas.JobAdvertisement
    belongs_to :framework, Grdev.TechnologyDomains.Schemas.Framework
  end
end
