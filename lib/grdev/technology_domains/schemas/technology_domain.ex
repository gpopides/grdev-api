defmodule Grdev.TechnologyDomains.Schemas.TechnologyDomain do
  use Ecto.Schema
  import Ecto.Changeset

  schema "technology_domains" do
    field :name, :string
    timestamps()
  end

  @doc false
  def changeset(technology_domain, attrs) do
    technology_domain
    |> cast(attrs, [])
    |> validate_required([])
  end
end
