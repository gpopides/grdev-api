defmodule Grdev.TechnologyDomains.Schemas.Framework do
  use Ecto.Schema
  import Ecto.Changeset

  alias Grdev.TechnologyDomains.Schemas.JobAdvertisementFramework
  alias Grdev.JobAdvertisements.Schemas.JobAdvertisement

  schema "frameworks" do
    field :name, :string

    many_to_many :advertisements,
                 JobAdvertisement,
                 join_through: JobAdvertisementFramework,
                 join_keys: [framework_id: :id, job_advertisement_id: :id]
  end

  @doc false
  def changeset(technolgy, attrs) do
    technolgy
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
