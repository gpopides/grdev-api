defmodule Grdev.TechnologyDomains.Schemas.JobAdvertisementLanguage do
  use Ecto.Schema

  alias Grdev.TechnologyDomains.Schemas.ProgrammingLanguage
  alias Grdev.JobAdvertisements.Schemas.JobAdvertisement

  @primary_key false
  schema "job_advertisement_languages" do
    belongs_to :programming_language, ProgrammingLanguage
    belongs_to :job_advertisement, JobAdvertisement
  end
end
