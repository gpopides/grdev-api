defmodule Grdev.TechnologyDomains.Schemas.ProgrammingLanguage do
  use Ecto.Schema
  import Ecto.Changeset

  alias Grdev.JobAdvertisements.Schemas.JobAdvertisement
  alias Grdev.TechnologyDomains.Schemas.JobAdvertisementLanguage

  schema "programming_languages" do
    field :name, :string

    many_to_many :advertisements,
                 JobAdvertisement,
                 join_through: JobAdvertisementLanguage,
                 join_keys: [programming_language_id: :id, job_advertisement_id: :id]
  end

  @doc false
  def changeset(programming_language, attrs) do
    programming_language
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
