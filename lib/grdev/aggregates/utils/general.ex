defmodule Grdev.Aggregates.Utils.GeneralUtils do
  @moduledoc """
  General utils that are used by Aggregates contexts
  """

  alias Grdev.Aggregates.Structs.RangedSalaryAggregate

  @doc """
  Takes a list of salaries and creates a list with RangedSalaryAggregate
  ## Examples
  iex> categorize_salaries([350, 400, 500, 600, 800, 1000, 1300])
  [%Grdev.Aggregates.Structs.RangedSalaryAggregate{range: %{lower: 200, upper: 400}, salaries: [200, 400]}, ...]
  """
  @spec categorize_salaries(salaries :: list(integer())) :: list(RangedSalaryAggregate.t())
  def categorize_salaries(salaries) when length(salaries) == 0, do: []

  def categorize_salaries(salaries) do
    chunk_count = Float.ceil(length(salaries) * 0.2) |> trunc()

    Stream.chunk_every(salaries, chunk_count)
    |> Enum.map(fn salaries_chunk ->
      lower = List.first(salaries_chunk)
      upper = List.last(salaries_chunk)
      RangedSalaryAggregate.new(lower, upper, salaries_chunk)
    end)
  end
end
