defmodule Grdev.Aggregates.Contexts.Aggregates do
  @moduledoc """
  The Aggregates context.
  This module is used for aggregates/statistics about job advertisements,
  technologies etc.
  """

  import Ecto.Query, warn: false
  alias Grdev.Repo

  alias Grdev.Aggregates.Structs.TechnologyAggregateResult
  alias Grdev.Aggregates.Structs.JobAdvertisementAggregate
  alias Grdev.Aggregates.Structs.SalaryAggregate
  alias Grdev.Aggregates.Structs.RangedSalaryAggregate

  alias Grdev.Aggregates.Repositories.AggregatesRepository
  alias Grdev.Aggregates.Utils.GeneralUtils
  alias Grdev.TechnologyDomains.Contexts.TechnologyDomain
  alias Grdev.JobAdvertisements.Schemas.Qualifications
  alias Grdev.JobAdvertisements.Contexts.JobAdvertisement

  @doc """
  Returns the number of job ads per framework and per programming language

  ## Examples
  iex> list_technology_aggregates()
    %{
      languages: [%TechnologyAggregateResult{}, ...],
      frameworks: [%TechnologyAggregateResult{}, ...]
    }
  """
  def list_technology_aggregates do
    TechnologyDomain.technologies_per_advertisements()
    |> TechnologyAggregateResult.from_pairs_list()
  end

  @doc """
  Returns stats for salaries


  ## Examples
  iex> salary_stats()
  %{"average_salary" => 1100, "max_lower_salary" => 1300, "max_upper_salary" => 1500}

  """
  @spec salary_stats() :: SalaryAggregate.t()
  def salary_stats() do
    q =
      from(
        q in Qualifications,
        select: %{
          "average_salary" =>
            fragment("avg(? + coalesce(?, ?))/2", q.lower_salary, q.upper_salary, q.lower_salary),
          "max_lower_salary" => fragment("max(?)", q.lower_salary),
          "max_upper_salary" => fragment("max(coalesce(?, ?))", q.upper_salary, q.lower_salary)
        }
      )

    all_salaries =
      from(
        q in Qualifications,
        select: %{
          "lower" => q.lower_salary,
          "upper" => q.upper_salary
        }
      )
      |> Repo.all()

    general_salary_stats = Repo.one(q)
    SalaryAggregate.new(general_salary_stats, all_salaries)
  end

  @doc """
  General stats about advertisements 

  ## Examples
  iex> advertisement_stats()
  %{total_count: 30, ads_per_city: [%JobAdvertisementAggregate{}, ...]}
  """
  @spec advertisement_stats() :: %{
          total_count: integer(),
          ads_per_city: list(JobAdvertisementAggregate.t())
        }
  def advertisement_stats() do
    %{
      total_count: JobAdvertisement.num_of_advertisements(),
      ads_per_city: JobAdvertisement.ads_per_city() |> Enum.map(&JobAdvertisementAggregate.new/1)
    }
  end

  @doc """
  Returns the salaries(lower or upper) grouped by count
  in a ranges manner
  ## Examples
  iex> grouped_salaries(:lower)
  [%RangedSalaryAggregate.t(), ...]
  """
  @spec range_grouped_salaries(key :: :lower | :upper) :: list(RangedSalaryAggregate.t()) | []
  def range_grouped_salaries("lower"), do: get_range_grouped_salaries("lower")
  def range_grouped_salaries("upper"), do: get_range_grouped_salaries("upper")
  def range_grouped_salaries(_), do: []

  defp get_range_grouped_salaries(key) do
    grouped_salaries = AggregatesRepository.grouped_salaries_query(key) |> Repo.all()

    grouped_salaries
    |> Stream.map(fn x -> Enum.fetch!(x, 0) end)
    |> Enum.to_list()
    |> GeneralUtils.categorize_salaries()
  end
end
