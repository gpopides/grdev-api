defmodule Grdev.Aggregates.Structs.TechnologyAggregateResult do
  defstruct [:name, :count]

  @type t() :: %__MODULE__{name: String.t(), count: integer()}

  alias __MODULE__

  @type language_demand_pair() :: [String.t() | integer()]
  @type framework_demand_pair() :: [String.t() | integer()]

  @spec new(list([String.t() | integer()])) ::
          {:ok, TechnologyAggregateResult.t()} | {:error, String.t()}
  def new([name, count]) when is_binary(name) when is_integer(count) do
    {:ok, %TechnologyAggregateResult{name: name, count: count}}
  end

  def new(_), do: {:error, "invalid data for TechnologyAggregateResult"}

  @spec empty() :: %TechnologyAggregateResult{}
  def empty(), do: %TechnologyAggregateResult{}

  @spec from_pairs_list({language_demand_pair(), framework_demand_pair()}) ::
          {[TechnologyAggregateResult.t()], [TechnologyAggregateResult.t()]}
  def from_pairs_list({language_pairs, framework_pairs}) do
    pairs_mapper = fn pairs ->
      Enum.map(pairs, fn pair ->
        with {:ok, result} <- new(pair) do
          result
        else
          {:error, _reason} -> TechnologyAggregateResult.empty()
        end
      end)
    end

    {pairs_mapper.(language_pairs), pairs_mapper.(framework_pairs)}
  end

  @spec toJson(struct :: t()) :: map()
  def toJson(struct = %TechnologyAggregateResult{}) do
    %{name: struct.name, count: struct.count}
  end

  def toJson(_), do: %{}
end
