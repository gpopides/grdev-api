defmodule Grdev.Aggregates.Structs.RangedSalaryAggregate do
  defstruct [:range, :salaries]

  alias __MODULE__

  @type t :: %RangedSalaryAggregate{range: map(), salaries: list(integer)}

  @spec new(
          lower_range_limit :: integer(),
          upper_range_limit :: integer(),
          salaries :: list(integer())
        ) :: t()
  def new(lower_range_limit, upper_range_limit, salaries) do
    %RangedSalaryAggregate{
      range: %{
        lower: lower_range_limit,
        upper: upper_range_limit
      },
      salaries: salaries
    }
  end

  def new(), do: empty()

  @spec empty() :: %RangedSalaryAggregate{range: nil, salaries: list()}
  def empty() do
    %RangedSalaryAggregate{salaries: []}
  end

  @spec toJson(struct :: RangedSalaryAggregate) :: map()
  def toJson(struct = %RangedSalaryAggregate{}) do
    %{
      range: %{
        lower: struct.range.lower,
        upper: struct.range.upper
      },
      salaries: struct.salaries
    }
  end

  def toJson(_), do: %{}
end
