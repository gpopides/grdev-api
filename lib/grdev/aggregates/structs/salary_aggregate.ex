defmodule Grdev.Aggregates.Structs.SalaryAggregate do
  defstruct [:max_lower_salary, :max_upper_salary, :average_salary, :salaries]

  alias __MODULE__

  @type t :: %SalaryAggregate{
          max_lower_salary: integer,
          max_upper_salary: integer,
          average_salary: integer,
          salaries: list(integer())
        }

  @spec new(
          db_result :: list(String.t() | integer()),
          all_salaries :: list(list(integer() | integer()))
        ) :: t()
  def new(db_result, all_salaries) when is_map(db_result) do
    # first add the statistics to the map and then finally add the salaries key too
    Enum.reduce(db_result, empty(), &db_result_reducer/2)
    |> Map.put(:salaries, all_salaries)
  end

  def new(_, _), do: empty()
  def new(), do: empty()

  @spec empty() :: %SalaryAggregate{}
  def empty(), do: %SalaryAggregate{}

  defp db_result_reducer({key, value}, acc) do
    set_salary_struct_value(acc, key, value)
  end

  @spec update_struct(struct :: t(), key :: String.t() | :atom, value :: any()) :: t()
  def update_struct(struct, key, value) when is_binary(key) do
    Map.put(struct, String.to_atom(key), value)
  end

  def update_struct(struct, key, value) when is_atom(key) do
    Map.put(struct, key, value)
  end

  # After calculating the averages from the DB, we need to "normalize" them because they may be nulls
  # so instead of returning average: nil, we should return average: 0.
  # The following method, does that.
  defp set_salary_struct_value(aggregate_struct, key, nil),
    do: update_struct(aggregate_struct, key, 0)

  defp set_salary_struct_value(salary_struct, key, value) when key == "average_salary" do
    Map.put(
      salary_struct,
      :average_salary,
      Decimal.round(value) |> Decimal.to_integer()
    )
  end

  defp set_salary_struct_value(salary_struct, key, value),
    do: Map.put(salary_struct, String.to_atom(key), value)

  @spec toJson(struct :: t()) :: map()
  def toJson(struct = %SalaryAggregate{}) do
    %{
      average: struct.average_salary,
      maxLowerSalary: struct.max_lower_salary,
      maxUpperSalary: struct.max_upper_salary,
      salaries: struct.salaries
    }
  end

  def toJson(_), do: %{}
end
