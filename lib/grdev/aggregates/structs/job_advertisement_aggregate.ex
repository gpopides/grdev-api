defmodule Grdev.Aggregates.Structs.JobAdvertisementAggregate do
  defstruct [:city, :count]

  alias __MODULE__

  @type t :: %JobAdvertisementAggregate{city: String.t() | nil, count: integer() | nil}

  @spec new(aggregate_pair :: list(String.t() | integer())) :: t()
  def new([city, count]) when is_binary(city) when is_integer(count) do
    %JobAdvertisementAggregate{
      city: city,
      count: count
    }
  end

  def new(_), do: empty()

  @spec new() :: t()
  def new(), do: empty()

  @spec empty() :: %JobAdvertisementAggregate{}
  def empty(), do: %JobAdvertisementAggregate{}

  @spec toJson(struct :: t()) :: map()
  def toJson(struct = %JobAdvertisementAggregate{}) do
    %{
      city: struct.city,
      count: struct.count
    }
  end

  def toJson(_), do: %{}
end
