defmodule Grdev.Aggregates.Repositories.AggregatesRepository do
  @moduledoc """
  This module contains functions which return queries
  that can be used by Ecto.Repo, in order to seperate the logic
  and don't fill the contexts with big parts that are mostly preparing queries
  """
  import Ecto.Query, warn: false

  alias Grdev.JobAdvertisements.Schemas.Qualifications

  @spec grouped_salaries_query(key :: String.t()) :: %Ecto.Query{}
  def grouped_salaries_query("lower"), do: salaries_grouped_by_lower_salary()
  def grouped_salaries_query("upper"), do: salaries_grouped_by_upper_salary()

  defp salaries_grouped_by_lower_salary do
    from(
      qual in Qualifications,
      group_by: [qual.lower_salary],
      select: [qual.lower_salary, count(qual.lower_salary)],
      order_by: qual.lower_salary
    )
  end

  defp salaries_grouped_by_upper_salary do
    from(
      qual in Qualifications,
      group_by: [qual.upper_salary],
      select: [qual.upper_salary, count(qual.upper_salary)],
      where: not is_nil(qual.upper_salary),
      order_by: qual.upper_salary
    )
  end
end
