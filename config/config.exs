# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :grdev,
  ecto_repos: [Grdev.Repo]

# Configures the endpoint
config :grdev, GrdevWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "XPs7WdItywK0jpg0rEgrTs/EtiYEDdCnCZl+ITqd2yF49U0zaPAYhEA91SxT9Y7K",
  render_errors: [view: GrdevWeb.ErrorView, accepts: ~w(json)],
  pubsub_server: Grdev.PubSub,
  live_view: [signing_salt: "iw6vAhMs"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
