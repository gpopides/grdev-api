use Mix.Config

# Configure your database
config :grdev, Grdev.Repo,
  username: System.get_env("POSTGRES_USER") || "admin",
  password: System.get_env("POSTGRES_PASSWORD") || "admin",
  database: System.get_env("POSTGRES_DB") || "grdev_api_test",
  hostname: System.get_env("POSTGRES_HOST") || "192.168.1.6",
  pool: Ecto.Adapters.SQL.Sandbox,
  port: System.get_env("POSTGRES_PORT") || 5859

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :grdev, GrdevWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

config :grdev, :phoenix_swagger,
  url: [host: "localhost:4000/swagger"],
  swagger_files: %{
    "priv/static/swagger.json" => [
      router: GrdevWeb.Router,
      endpoint: GrdevWeb.Endpoint
    ]
  }

config :phoenix_swagger, json_library: Jason
