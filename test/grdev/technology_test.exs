defmodule Grdev.TechnologyTest do
  use Grdev.DataCase

  alias Grdev.TechnologyDomains.Schemas.ProgrammingLanguage
  alias Grdev.TechnologyDomains.Schemas.Framework
  alias Grdev.TechnologyDomains.Contexts.TechnologyDomain

  test "list_programming_lanagues/0 returns all programming languages" do
    assert length(TechnologyDomain.list_programming_languages()) == 14
  end

  test "list_frameworks/0 returns all frameworks" do
    assert length(TechnologyDomain.list_programming_languages()) == 14
  end

  test "list_technologies/0 returns frameworks and languages" do
    languages = ProgrammingLanguage |> order_by(asc: :name) |> Repo.all()
    frameworks = Framework |> order_by(asc: :name) |> Repo.all()

    assert TechnologyDomain.list_technologies() == %{
             languages: languages,
             frameworks: frameworks
           }
  end
end
