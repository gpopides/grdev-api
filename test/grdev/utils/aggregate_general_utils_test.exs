defmodule Grdev.AggregateGeneralUtilTest do
  use ExUnit.Case

  alias Grdev.Aggregates.Utils.GeneralUtils
  alias Grdev.Aggregates.Structs.RangedSalaryAggregate

  test "categorize_salaries/1 with empty salaries list returns empty map" do
    assert [] == GeneralUtils.categorize_salaries([])
  end

  test "categorize_salaries/1 with non empty salaries length = 4 returns 4 RangedSalaryAggregate" do
    expected_result = [
      %RangedSalaryAggregate{range: %{lower: 200, upper: 200}, salaries: [200]},
      %RangedSalaryAggregate{range: %{lower: 400, upper: 400}, salaries: [400]},
      %RangedSalaryAggregate{range: %{lower: 300, upper: 300}, salaries: [300]},
      %RangedSalaryAggregate{range: %{lower: 500, upper: 500}, salaries: [500]}
    ]

    assert expected_result == GeneralUtils.categorize_salaries([200, 400, 300, 500])
  end

  test "categorize_salaries/1 with non empty salaries length = 6 returns 3 categories" do
    expected_result = [
      %RangedSalaryAggregate{range: %{lower: 200, upper: 400}, salaries: [200, 400]},
      %RangedSalaryAggregate{range: %{lower: 300, upper: 500}, salaries: [300, 500]},
      %RangedSalaryAggregate{range: %{lower: 350, upper: 550}, salaries: [350, 550]}
    ]

    assert expected_result == GeneralUtils.categorize_salaries([200, 400, 300, 500, 350, 550])
  end
end
