defmodule Grdev.AggregateStructTest do
  use ExUnit.Case

  alias Grdev.Aggregates.Structs.RangedSalaryAggregate
  alias Grdev.Aggregates.Structs.SalaryAggregate
  alias Grdev.Aggregates.Structs.JobAdvertisementAggregate
  alias Grdev.Aggregates.Structs.TechnologyAggregateResult

  test "RangedSalaryAggregate.new/1 returns empty" do
    assert RangedSalaryAggregate.new() == %RangedSalaryAggregate{range: nil, salaries: []}
  end

  test "RangedSalaryAggregate.new/1 returns new struct" do
    expected = %RangedSalaryAggregate{
      range: %{
        lower: 100,
        upper: 500
      },
      salaries: [100, 200, 400, 500, 350]
    }

    assert expected == RangedSalaryAggregate.new(100, 500, [100, 200, 400, 500, 350])
  end

  test "SalaryAggregate.new/0 returns empy" do
    assert %SalaryAggregate{} == SalaryAggregate.new()
  end

  test "SalaryAggregate.new/1 with invalid param returns empy" do
    assert %SalaryAggregate{} == SalaryAggregate.new([], [])
  end

  test "TechnologyAggregateResult.new/1 with invalid param" do
    assert {:error, "invalid data for TechnologyAggregateResult"} ==
             TechnologyAggregateResult.new([1, "asd"])
  end

  test "TechnologyAggregateResult.new/1 with valid param" do
    assert {:ok, %TechnologyAggregateResult{name: "Name", count: 5}} ==
             TechnologyAggregateResult.new(["Name", 5])
  end

  test "SalaryAggregate.new/1 with valid param returns struct" do
    assert %SalaryAggregate{
             max_lower_salary: 1000,
             max_upper_salary: 1000,
             average_salary: 1000,
             salaries: [[1000, 1000], [1000, 1000], [1000, 1000]]
           } ==
             SalaryAggregate.new(
               %{
                 "average_salary" => 1000,
                 "max_lower_salary" => 1000,
                 "max_upper_salary" => 1000
               },
               [[1000, 1000], [1000, 1000], [1000, 1000]]
             )
  end

  test "JobAdvertisementAggregate.new/0 returns empty" do
    assert %JobAdvertisementAggregate{} == JobAdvertisementAggregate.new()
  end

  test "JobAdvertisementAggregate.new/2 invalid params returns empty" do
    assert %JobAdvertisementAggregate{} == JobAdvertisementAggregate.new([1, "sad"])
  end

  test "JobAdvertisementAggregate.new/2 valid params  returns struct" do
    assert %JobAdvertisementAggregate{city: "City", count: 1} ==
             JobAdvertisementAggregate.new(["City", 1])
  end

  test "TechnologyAggregateResult.toJson" do
    x = %TechnologyAggregateResult{name: "Name", count: 1}
    assert %{name: "Name", count: 1} == TechnologyAggregateResult.toJson(x)
  end

  test "JobAdvertisementAggregate.toJson" do
    x = %JobAdvertisementAggregate{city: "City", count: 1}
    assert %{city: "City", count: 1} == JobAdvertisementAggregate.toJson(x)
  end

  test "SalaryAggregate.toJson" do
    x = %SalaryAggregate{
      max_lower_salary: 1000,
      max_upper_salary: 1000,
      average_salary: 1000,
      salaries: [1000, 1000]
    }

    assert %{maxLowerSalary: 1000, maxUpperSalary: 1000, average: 1000, salaries: [1000, 1000]} ==
             SalaryAggregate.toJson(x)
  end

  test "RangedSalaryAggregate.toJson" do
    x = %RangedSalaryAggregate{
      range: %{
        lower: 100,
        upper: 500
      },
      salaries: [100, 200, 400, 500, 350]
    }

    assert %{
             range: %{
               lower: 100,
               upper: 500
             },
             salaries: [100, 200, 400, 500, 350]
           } ==
             RangedSalaryAggregate.toJson(x)
  end

  test "SalaryAggregate.toJson invalid data returns empty map" do
    assert %{} == SalaryAggregate.toJson(%{})
  end

  test "JobAdvertisementAggregate.toJson invalid data returns empty map" do
    assert %{} == JobAdvertisementAggregate.toJson(%{})
  end

  test "RangedSalaryAggregate.toJson invalid data returns empty map" do
    assert %{} == RangedSalaryAggregate.toJson(%{})
  end

  test "TechnologyAggregateResult.toJson invalid data returns empty map" do
    assert %{} == TechnologyAggregateResult.toJson(%{})
  end
end
