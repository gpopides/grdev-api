defmodule Grdev.GenServers.AdvertisementsCacheTest do
  use ExUnit.Case, async: true

  alias Grdev.Caches.AdvertisementCache

  test "AdvertisementCache.find/1 returns nil because key not stored" do
    assert {:error, :not_found} == AdvertisementCache.find("non existing key")
  end

  test "AdvertisementCache.find/1 with page filter only" do
    value_key_params = %{"page" => 1}
    AdvertisementCache.insert(value_key_params, "item")
    assert {:ok, "item"} == AdvertisementCache.find(value_key_params)
  end

  test "AdvertisementCache.find/1 with page and frameworks filter " do
    value_key_params = %{"page" => 1, "frameworks" => [1, 2, 3]}
    AdvertisementCache.insert(value_key_params, "item")
    assert {:ok, "item"} == AdvertisementCache.find(value_key_params)
  end

  test "AdvertisementCache.find/1 with page and lagnauges filter " do
    value_key_params = %{"page" => 1, "languages" => [1, 2, 3]}
    AdvertisementCache.insert(value_key_params, "item")
    assert {:ok, "item"} == AdvertisementCache.find(value_key_params)
  end

  test "AdvertisementCache.find/1 with page lagnauges and frameworks filter " do
    value_key_params = %{"page" => 1, "languages" => [1, 2, 3], "frameworks" => [1, 2, 3]}
    AdvertisementCache.insert(value_key_params, "item")
    assert {:ok, "item"} == AdvertisementCache.find(value_key_params)
  end

  @tag :tmp
  test "AdvertisementCache.insert/2 with cache limit + 1 replaces first cache insert" do
    Enum.each(0..41, fn page ->
      value_key_params = %{"page" => page, "languages" => [1, 2, 3], "frameworks" => [1, 2, 3]}
      AdvertisementCache.insert(value_key_params, "item")
    end)

    deleted_key_params = %{"page" => 0, "languages" => [1, 2, 3], "frameworks" => [1, 2, 3]}
    assert {:error, :not_found} == AdvertisementCache.find(deleted_key_params)
  end
end
