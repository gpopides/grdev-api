defmodule Grdev.JobAdvertisementsTest do
  use Grdev.DataCase

  alias Grdev.JobAdvertisements.Contexts.JobAdvertisement
  alias Grdev.JobAdvertisements.Schemas.Poster
  alias Grdev.TechnologyDomains.Schemas.ProgrammingLanguage
  alias Grdev.TechnologyDomains.Schemas.Framework
  alias Grdev.JobAdvertisements.Schemas.Company

  alias Grdev.Caches.AdvertisementCache

  setup do
    :ok
  end

  describe "job_advertisements" do
    @valid_attrs %{
      title: "Full stack developer",
      description: "some description",
      city: "Thesaloniki",
      poster: %{
        name: "Poster name",
        email: "email",
        company: %{
          name: "company a",
          type: "software",
          location: "Some location"
        }
      },
      programming_languages: [],
      frameworks: [],
      qualifications: %{
        lower_salary: 2000,
        lower_years_of_experience: 1
      }
    }
    @invalid_attrs %{}

    def job_advertisement_fixture(attrs \\ %{}) do
      {:ok, job_advertisement} =
        attrs
        |> Enum.into(@valid_attrs)
        |> JobAdvertisement.create_job_advertisement()

      job_advertisement
    end

    test "list_job_advertisements/1 without filters returns all job_advertisements" do
      AdvertisementCache.remove(%{"page" => 1})
      job_advertisement = job_advertisement_fixture()

      assert {:ok, job_advertisements, total_pages_count} =
               JobAdvertisement.list_job_advertisements(%{"page" => 1})

      assert length(job_advertisements) == 1
      assert total_pages_count == 1
      assert job_advertisements == [job_advertisement]
      AdvertisementCache.remove(%{"page" => 1})
    end

    test "list_job_advertisements/1 with negative page returns error" do
      assert {:error, message} = JobAdvertisement.list_job_advertisements(%{"page" => -1})
      assert message == "page can not have a negative value"
    end

    test "list_job_advertisements/1 with invalid page returns error" do
      assert {:error, message} = JobAdvertisement.list_job_advertisements(%{"page" => "invalid"})
      assert message == "invalid page"
    end

    test "list_job_advertisements/1 with framework filters returns advertisements which use given frameworks" do
      job_advertisement_fixtures =
        Enum.map([1, 2], fn framework_id ->
          job_advertisement_fixture(%{frameworks: [framework_id]})
        end)

      params = %{"page" => 1, "framework_ids" => [1]}

      assert {:ok, job_advertisements, total_pages_count} =
               JobAdvertisement.list_job_advertisements(params)

      assert total_pages_count == 1
      assert length(job_advertisements) == 1
      assert job_advertisements == [Enum.fetch!(job_advertisement_fixtures, 0)]
    end

    test "list_job_advertisements/1 with language filters returns advertisements which use given language" do
      job_advertisement_fixtures =
        Enum.map([1, 2], fn language_id ->
          job_advertisement_fixture(%{programming_languages: [language_id]})
        end)

      params = %{"language_ids" => [2], "page" => 1}
      AdvertisementCache.remove(params)

      assert {:ok, job_advertisements, total_pages_count} =
               JobAdvertisement.list_job_advertisements(params)

      assert length(job_advertisements) == 1
      assert total_pages_count == 1
      assert job_advertisements == [Enum.fetch!(job_advertisement_fixtures, 1)]
    end

    test "list_job_advertisements/1 with language and framework filters returns job advertisement that has both" do
      advertisement = job_advertisement_fixture(%{frameworks: [1], programming_languages: [2]})

      params = %{
        "language_ids" => [2],
        "framework_ids" => [1],
        "page" => 1
      }

      AdvertisementCache.remove(params)

      assert {:ok, job_advertisements, total_pages_count} =
               JobAdvertisement.list_job_advertisements(params)

      assert length(job_advertisements) == 1
      assert total_pages_count == 1
      assert job_advertisements == [advertisement]
    end

    test "get_job_advertisement!/1 returns the job_advertisement with given id" do
      job_advertisement = job_advertisement_fixture()
      assert JobAdvertisement.get_job_advertisement(job_advertisement.id) == job_advertisement
    end

    test "create_job_advertisement/1 with valid data creates a job_advertisement" do
      assert {:ok, %Grdev.JobAdvertisements.Schemas.JobAdvertisement{} = job_advertisement} =
               JobAdvertisement.create_job_advertisement(%{
                 @valid_attrs
                 | programming_languages: [1],
                   frameworks: [1]
               })

      assert %Poster{} = job_advertisement.poster
      assert %Company{} = job_advertisement.poster.company
      assert "Full stack developer" == job_advertisement.title

      assert length(job_advertisement.programming_languages) == 1
      assert length(job_advertisement.frameworks) == 1

      assert {:ok, %Framework{name: "Spring Boot"}} = Enum.fetch(job_advertisement.frameworks, 0)

      assert {:ok, %ProgrammingLanguage{name: "Java"}} =
               Enum.fetch(job_advertisement.programming_languages, 0)
    end

    test "create_job_advertisement/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} =
               JobAdvertisement.create_job_advertisement(@invalid_attrs)
    end

    test "create_job_advertisement/1 fails with lower_salary greater than upper salary fails" do
      data = %{
        title: "Full stack developer",
        description: "some description",
        city: "Thesaloniki",
        poster: %{name: "Poster name", email: "email"},
        programming_languages: [1],
        frameworks: [1],
        qualifications: %{
          "lower_salary" => 2000,
          "upper_salary" => 1000,
          "lower_years_of_experience" => 1
        }
      }

      assert {:error, %Ecto.Changeset{}} = JobAdvertisement.create_job_advertisement(data)
    end

    test "create_job_advertisement/1 fails with lower salary exceeding limit 100.000" do
      data = %{
        title: "Full stack developer",
        description: "some description",
        city: "Thesaloniki",
        poster: %{name: "Poster name", email: "email"},
        programming_languages: [1],
        frameworks: [1],
        qualifications: %{
          "lower_salary" => 100_001,
          "lower_years_of_experience" => 1
        }
      }

      assert {:error, %Ecto.Changeset{}} = JobAdvertisement.create_job_advertisement(data)
    end

    test "create_job_advertisement/1 fails with upper salary exceeding limit 100.000" do
      data = %{
        title: "Full stack developer",
        description: "some description",
        city: "Thesaloniki",
        poster: %{name: "Poster name", email: "email"},
        programming_languages: [1],
        frameworks: [1],
        qualifications: %{
          "lower_salary" => 1,
          "upper_salary" => 100_001,
          "lower_years_of_experience" => 1
        }
      }

      assert {:error, %Ecto.Changeset{}} = JobAdvertisement.create_job_advertisement(data)
    end
  end
end
