defmodule Grdev.AggregateTest do
  use Grdev.DataCase

  alias Grdev.Aggregates.Contexts.Aggregates
  alias Grdev.Aggregates.Structs.TechnologyAggregateResult
  alias Grdev.Aggregates.Structs.JobAdvertisementAggregate
  alias Grdev.Aggregates.Structs.SalaryAggregate
  alias Grdev.Aggregates.Structs.RangedSalaryAggregate

  alias Grdev.JobAdvertisementsTest

  test "list_technologies_aggregates/0 returns aggregate result structs" do
    JobAdvertisementsTest.job_advertisement_fixture(%{
      programming_languages: [1, 2],
      frameworks: [1, 2]
    })

    assert {language_results, framework_results} = Aggregates.list_technology_aggregates()

    type_asserter = fn item -> assert true == item_is_aggregate_result?(item) end

    Enum.each(language_results, type_asserter)
    Enum.each(framework_results, type_asserter)
  end

  test "list_technologies_aggregates/0 returns correct aggregate results" do
    JobAdvertisementsTest.job_advertisement_fixture(%{
      programming_languages: [1, 2],
      frameworks: [1, 2]
    })

    assert {language_results, framework_results} = Aggregates.list_technology_aggregates()

    Enum.each(language_results, fn x ->
      if x.name == "Java" || x.name == "Kotlin" do
        assert x.count == 1
      else
        assert x.count == 0
      end
    end)

    Enum.each(framework_results, fn x ->
      if x.name == "Phoenix" || x.name == "Spring Boot" do
        assert x.count == 1
      else
        assert x.count == 0
      end
    end)
  end

  test "salary_stats/0 returns correct stats" do
    salaries = [{1000, 1200}, {1300, 1500}, {800, nil}]

    Enum.each(salaries, fn {lower, upper} ->
      JobAdvertisementsTest.job_advertisement_fixture(%{
        qualifications: %{lower_salary: lower, upper_salary: upper, lower_years_of_experience: 1}
      })
    end)

    assert %SalaryAggregate{
             average_salary: 1100,
             max_lower_salary: 1300,
             max_upper_salary: 1500,
             salaries: [
               %{"lower" => 1000, "upper" => 1200},
               %{"lower" => 1300, "upper" => 1500},
               %{"lower" => 800, "upper" => 800}
             ]
           } ==
             Aggregates.salary_stats()
  end

  test "advertisement_stats/0 without stored advertisement" do
    expected_result = %{total_count: 0, ads_per_city: []}
    assert expected_result == Aggregates.advertisement_stats()
  end

  test "advertisement_stats/0 with 5 stored advertisement" do
    for _ <- 1..5 do
      JobAdvertisementsTest.job_advertisement_fixture()
    end

    expected_result = %{
      total_count: 5,
      ads_per_city: [%JobAdvertisementAggregate{count: 5, city: "Thesaloniki"}]
    }

    assert expected_result == Aggregates.advertisement_stats()
  end

  test "advertisement_stats/0 with 2 stored advertisement with different city" do
    for city <- ["A", "B"] do
      JobAdvertisementsTest.job_advertisement_fixture(%{city: city})
    end

    expected_result = %{
      total_count: 2,
      ads_per_city: [
        %JobAdvertisementAggregate{count: 1, city: "A"},
        %JobAdvertisementAggregate{count: 1, city: "B"}
      ]
    }

    assert expected_result == Aggregates.advertisement_stats()
  end

  test "range_grouped_salaries/1 for lower returns empty" do
    assert [] == Aggregates.range_grouped_salaries(:lower)
  end

  test "range_grouped_salaries/1 for upper returns empty" do
    assert [] == Aggregates.range_grouped_salaries(:upper)
  end

  test "range_grouped_salaries/1 for lower returns salaries in 4 categories" do
    salaries = [
      {1000, 1200},
      {1300, 1500},
      {800, nil},
      {400, 1000},
      {500, 1000},
      {600, 1000},
      {350, 1000}
    ]

    Enum.each(salaries, fn {lower, upper} ->
      JobAdvertisementsTest.job_advertisement_fixture(%{
        qualifications: %{lower_salary: lower, upper_salary: upper, lower_years_of_experience: 1}
      })
    end)

    expected_result = [
      %RangedSalaryAggregate{range: %{lower: 350, upper: 400}, salaries: [350, 400]},
      %RangedSalaryAggregate{range: %{lower: 500, upper: 600}, salaries: [500, 600]},
      %RangedSalaryAggregate{range: %{lower: 800, upper: 1000}, salaries: [800, 1000]},
      %RangedSalaryAggregate{range: %{lower: 1300, upper: 1300}, salaries: [1300]}
    ]

    assert expected_result == Aggregates.range_grouped_salaries("lower")
  end

  test "range_grouped_salaries/1 for upper returns salaries in 5 categories" do
    salaries = [{1000, 1200}, {1300, 1500}, {800, 1000}, {1000, 1100}, {1000, 1050}, {1000, 1020}]

    Enum.each(salaries, fn {lower, upper} ->
      JobAdvertisementsTest.job_advertisement_fixture(%{
        qualifications: %{lower_salary: lower, upper_salary: upper, lower_years_of_experience: 1}
      })
    end)

    expected_result = [
      %RangedSalaryAggregate{range: %{lower: 1000, upper: 1020}, salaries: [1000, 1020]},
      %RangedSalaryAggregate{range: %{lower: 1050, upper: 1100}, salaries: [1050, 1100]},
      %RangedSalaryAggregate{range: %{lower: 1200, upper: 1500}, salaries: [1200, 1500]}
    ]

    assert expected_result == Aggregates.range_grouped_salaries("upper")
  end

  test "range_grouped_salaries/1 returns error because of invalid key" do
    assert [] == Aggregates.range_grouped_salaries("invalid")
  end

  defp item_is_aggregate_result?(%TechnologyAggregateResult{name: _name, count: _count}), do: true
  defp item_is_aggregate_result?(_item), do: false
end
