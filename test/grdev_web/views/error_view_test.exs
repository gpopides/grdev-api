defmodule GrdevWeb.ErrorViewTest do
  use GrdevWeb.ConnCase, async: true

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders 404.json" do
    assert render(GrdevWeb.ErrorView, "404.json", []) == %{error: "not found"}
  end

  test "renders 500.json" do
    assert render(GrdevWeb.ErrorView, "500.json", []) ==
             %{errors: %{detail: "Internal Server Error"}}
  end
end
