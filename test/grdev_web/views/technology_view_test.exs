defmodule GrdevWeb.TechnologyViewTest do
  use GrdevWeb.ConnCase, async: true

  alias Grdev.TechnologyDomains.Schemas.ProgrammingLanguage
  alias Grdev.TechnologyDomains.Schemas.Framework

  import Phoenix.View

  test "renders language.json" do
    assert render(GrdevWeb.TechnologyView, "language.json", %{
             technology: %ProgrammingLanguage{name: "somename", id: 1}
           }) == %{name: "somename", id: 1}
  end

  test "renders framework.json" do
    assert render(GrdevWeb.TechnologyView, "language.json", %{
             technology: %Framework{name: "somename", id: 1}
           }) == %{name: "somename", id: 1}
  end
end
