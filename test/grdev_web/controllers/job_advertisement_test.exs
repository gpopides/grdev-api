defmodule GrdevWeb.JobAdvertisementControllerTest do
  use GrdevWeb.ConnCase

  alias Grdev.JobAdvertisements.Contexts.JobAdvertisement
  alias Grdev.Caches.AdvertisementCache

  @valid_attrs %{
    title: "Full stack developer",
    description: "some description",
    city: "Thessaloniki",
    poster: %{
      name: "Poster name",
      email: "email",
      company: %{
        name: "company a",
        type: "software",
        location: "Some location"
      }
    },
    programming_languages: [1],
    frameworks: [1],
    qualifications: %{
      "lower_salary" => 2000,
      "lower_years_of_experience" => 1
    },
    benefits: "benefit1,benefit2"
  }
  # @update_attrs %{}
  @invalid_attrs %{}

  def job_advertisement_fixture(attrs \\ %{}) do
    {:ok, job_advertisement} =
      attrs
      |> Enum.into(@valid_attrs)
      |> JobAdvertisement.create_job_advertisement()

    job_advertisement
  end

  test "#create creates new advertisement" do
    response =
      build_conn(:post, "/api/job_advertisements", @valid_attrs) |> GrdevWeb.Endpoint.call([])

    response_body = Jason.decode!(response.resp_body)

    assert %{"id" => id} = response_body

    expected_result = %{
      "id" => id,
      "title" => "Full stack developer",
      "description" => "some description",
      "poster" => %{
        "name" => "Poster name",
        "email" => "email",
        "company" => %{
          "name" => "company a",
          "type" => "software",
          "location" => "Some location"
        }
      },
      "programmingLanguages" => [%{"id" => 1, "name" => "Java"}],
      "frameworks" => [%{"id" => 1, "name" => "Spring Boot"}],
      "qualifications" => %{
        "lowerSalary" => 2000,
        "upperSalary" => 2000,
        "lowerYearsOfExperience" => 1,
        "upperYearsOfExperience" => nil
      },
      "benefits" => ["benefit1", "benefit2"],
      "city" => "Thessaloniki"
    }

    assert 200 == response.status
    assert expected_result == response_body
  end

  test "#create creates new advertisement with completed all qualifications and empty benefits" do
    body = %{
      title: "Full stack developer",
      description: "some description",
      city: "Thessaloniki",
      poster: %{
        name: "Poster name",
        email: "email",
        company: %{
          name: "company a",
          type: "software",
          location: "Some location"
        }
      },
      programming_languages: [1],
      frameworks: [1],
      qualifications: %{
        "lower_salary" => 2000,
        "upper_salary" => 3000,
        "lower_years_of_experience" => 1,
        "upper_years_of_experience" => 2
      }
    }

    response = build_conn(:post, "/api/job_advertisements", body) |> GrdevWeb.Endpoint.call([])

    response_body = Jason.decode!(response.resp_body)

    assert %{"id" => id} = response_body

    expected_result = %{
      "id" => id,
      "title" => "Full stack developer",
      "description" => "some description",
      "poster" => %{
        "name" => "Poster name",
        "email" => "email",
        "company" => %{
          "name" => "company a",
          "type" => "software",
          "location" => "Some location"
        }
      },
      "programmingLanguages" => [%{"id" => 1, "name" => "Java"}],
      "frameworks" => [%{"id" => 1, "name" => "Spring Boot"}],
      "qualifications" => %{
        "lowerSalary" => 2000,
        "upperSalary" => 3000,
        "lowerYearsOfExperience" => 1,
        "upperYearsOfExperience" => 2
      },
      "benefits" => [],
      "city" => "Thessaloniki"
    }

    assert 200 == response.status

    assert Jason.encode!(expected_result) == response.resp_body
  end

  test "#create creates new advertisement with kotlin as only language" do
    body = %{
      title: "Full stack developer",
      description: "some description",
      city: "Thessaloniki",
      poster: %{
        name: "Poster name",
        email: "email",
        company: %{
          name: "company a",
          type: "software",
          location: "Some location"
        }
      },
      programming_languages: [2],
      frameworks: [],
      qualifications: %{
        "lower_salary" => 2000,
        "upper_salary" => 3000,
        "lower_years_of_experience" => 1,
        "upper_years_of_experience" => 2
      }
    }

    response = build_conn(:post, "/api/job_advertisements", body) |> GrdevWeb.Endpoint.call([])

    response_body = Jason.decode!(response.resp_body)

    assert %{"id" => id} = response_body

    expected_result = %{
      "id" => id,
      "title" => "Full stack developer",
      "description" => "some description",
      "poster" => %{
        "name" => "Poster name",
        "email" => "email",
        "company" => %{
          "name" => "company a",
          "type" => "software",
          "location" => "Some location"
        }
      },
      "programmingLanguages" => [%{"name" => "Kotlin", "id" => 2}],
      "frameworks" => [],
      "qualifications" => %{
        "lowerSalary" => 2000,
        "upperSalary" => 3000,
        "lowerYearsOfExperience" => 1,
        "upperYearsOfExperience" => 2
      },
      "benefits" => [],
      "city" => "Thessaloniki"
    }

    assert 200 == response.status

    assert Jason.encode!(expected_result) == response.resp_body
  end

  test "#create creates new advertisement with invalid languages & frameworks" do
    body = %{
      title: "Full stack developer",
      description: "some description",
      city: "Thessaloniki",
      poster: %{
        name: "Poster name",
        email: "email",
        company: %{
          name: "company a",
          type: "software",
          location: "Some location"
        }
      },
      programming_languages: [1200],
      frameworks: [12000],
      qualifications: %{
        "lower_salary" => 2000,
        "upper_salary" => 3000,
        "lower_years_of_experience" => 1,
        "upper_years_of_experience" => 2
      }
    }

    response = build_conn(:post, "/api/job_advertisements", body) |> GrdevWeb.Endpoint.call([])

    response_body = Jason.decode!(response.resp_body)

    assert %{"id" => id} = response_body

    expected_result = %{
      "id" => id,
      "title" => "Full stack developer",
      "description" => "some description",
      "poster" => %{
        "name" => "Poster name",
        "email" => "email",
        "company" => %{
          "name" => "company a",
          "type" => "software",
          "location" => "Some location"
        }
      },
      "programmingLanguages" => [],
      "frameworks" => [],
      "qualifications" => %{
        "lowerSalary" => 2000,
        "upperSalary" => 3000,
        "lowerYearsOfExperience" => 1,
        "upperYearsOfExperience" => 2
      },
      "benefits" => [],
      "city" => "Thessaloniki"
    }

    assert 200 == response.status

    assert Jason.encode!(expected_result) == response.resp_body
  end

  test "#create fails because missing advertisement body" do
    response =
      build_conn(:post, "/api/job_advertisements", @invalid_attrs) |> GrdevWeb.Endpoint.call([])

    assert 400 == response.status
    assert Jason.encode!(%{error: "missing advertisement data"}) == response.resp_body
  end

  test "#create fails because missing qualifications" do
    body = %{
      title: "Full stack developer",
      description: "some description",
      city: "Thesaloniki",
      poster: %{
        name: "Poster name",
        email: "email",
        company: %{
          name: "company a",
          type: "software",
          location: "Some location"
        }
      },
      programming_languages: [1],
      frameworks: [1]
    }

    response = build_conn(:post, "/api/job_advertisements", body) |> GrdevWeb.Endpoint.call([])

    expected_body = %{
      error: %{
        qualifications: ["can't be blank"]
      }
    }

    assert 400 == response.status
    assert Jason.encode!(expected_body) == response.resp_body
  end

  test "returns all job advertisements" do
    AdvertisementCache.remove(%{"page" => 1})
    job_advertisement_fixture()

    response = build_conn(:get, "/api/job_advertisements?page=1") |> GrdevWeb.Endpoint.call([])

    response_body = Jason.decode!(response.resp_body)

    assert %{"advertisements" => advertisements, "count" => total_count} = response_body
    assert length(advertisements) == 1
    assert total_count == 1
    assert 200 == response.status
  end

  test "list advertisements returns error because of invalid page parameter" do
    job_advertisement_fixture()

    response =
      build_conn(:get, "/api/job_advertisements?page=invalid") |> GrdevWeb.Endpoint.call([])

    response_body = Jason.decode!(response.resp_body)

    assert %{"error" => "invalid page"} = response_body
    assert 400 == response.status
  end

  test "#create fails because lower salary is greater than upper salary" do
    body = %{
      title: "Full stack developer",
      description: "some description",
      city: "Thessaloniki",
      poster: %{
        name: "Poster name",
        email: "email",
        company: %{
          name: "company a",
          type: "software",
          location: "Some location"
        }
      },
      programming_languages: [1],
      frameworks: [1],
      qualifications: %{
        "lower_salary" => 2000,
        "upper_salary" => 1000,
        "lower_years_of_experience" => 1
      },
      benefits: "benefit1,benefit2"
    }

    response = build_conn(:post, "/api/job_advertisements", body) |> GrdevWeb.Endpoint.call([])

    expected_body = %{
      error: %{
        qualifications: %{
          lower_salary: ["cant be greater than upper salary"]
        }
      }
    }

    assert 400 == response.status
    assert Jason.encode!(expected_body) == response.resp_body
  end

  test "#create fails because lower salary exceeds limit" do
    body = %{
      title: "Full stack developer",
      description: "some description",
      city: "Thessaloniki",
      poster: %{
        name: "Poster name",
        email: "email",
        company: %{
          name: "company a",
          type: "software",
          location: "Some location"
        }
      },
      programming_languages: [1],
      frameworks: [1],
      qualifications: %{
        "lower_salary" => 100_001,
        "lower_years_of_experience" => 1
      },
      benefits: "benefit1,benefit2"
    }

    response = build_conn(:post, "/api/job_advertisements", body) |> GrdevWeb.Endpoint.call([])

    expected_body = %{
      error: %{
        qualifications: %{
          lower_salary: ["Can't be greater than 100.000"]
        }
      }
    }

    assert 400 == response.status
    assert Jason.encode!(expected_body) == response.resp_body
  end

  test "#create fails because upper salary exceeds limit" do
    body = %{
      title: "Full stack developer",
      description: "some description",
      city: "Thessaloniki",
      poster: %{
        name: "Poster name",
        email: "email",
        company: %{
          name: "company a",
          type: "software",
          location: "Some location"
        }
      },
      programming_languages: [1],
      frameworks: [1],
      qualifications: %{
        "lower_salary" => 1,
        "upper_salary" => 100_001,
        "lower_years_of_experience" => 1
      },
      benefits: "benefit1,benefit2"
    }

    response = build_conn(:post, "/api/job_advertisements", body) |> GrdevWeb.Endpoint.call([])

    expected_body = %{
      error: %{
        qualifications: %{
          upper_salary: ["Can't be greater than 100.000"]
        }
      }
    }

    assert 400 == response.status
    assert Jason.encode!(expected_body) == response.resp_body
  end

  test "#create fails because poster company is missing" do
    body = %{
      title: "Full stack developer",
      description: "some description",
      city: "Thessaloniki",
      poster: %{
        name: "Poster name",
        email: "email"
      },
      programming_languages: [1],
      frameworks: [1],
      qualifications: %{
        "lower_salary" => 1,
        "upper_salary" => 2,
        "lower_years_of_experience" => 1
      },
      benefits: "benefit1,benefit2"
    }

    response = build_conn(:post, "/api/job_advertisements", body) |> GrdevWeb.Endpoint.call([])

    expected_body = %{
      error: %{
        poster: %{
          company: ["can't be blank"]
        }
      }
    }

    assert 400 == response.status
    assert Jason.encode!(expected_body) == response.resp_body
  end

  test "#create fails because poster company is missing data" do
    body = %{
      title: "Full stack developer",
      description: "some description",
      city: "Thessaloniki",
      poster: %{
        name: "Poster name",
        email: "email",
        company: %{}
      },
      programming_languages: [1],
      frameworks: [1],
      qualifications: %{
        "lower_salary" => 1,
        "upper_salary" => 2,
        "lower_years_of_experience" => 1
      },
      benefits: "benefit1,benefit2"
    }

    response = build_conn(:post, "/api/job_advertisements", body) |> GrdevWeb.Endpoint.call([])

    expected_body = %{
      error: %{
        poster: %{
          company: %{
            name: ["can't be blank"],
            location: ["can't be blank"],
            type: ["can't be blank"]
          }
        }
      }
    }

    assert 400 == response.status
    assert Jason.encode!(expected_body) == response.resp_body
  end
end
