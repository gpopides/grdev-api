defmodule GrdevWeb.AggregateControllerTest do
  use GrdevWeb.ConnCase

  alias Grdev.JobAdvertisementsTest

  describe "Aggregate Controller tests " do
    test "#index_salary_stats without advertisements" do
      response = build_conn(:get, "/api/aggregates/salary") |> GrdevWeb.Endpoint.call([])
      response_body = Jason.decode!(response.resp_body)
      assert %{"average" => 0, "maxLowerSalary" => 0, "maxUpperSalary" => 0} = response_body
    end

    test "#index_salary_stats with default advertisements fixture" do
      JobAdvertisementsTest.job_advertisement_fixture()
      response = build_conn(:get, "/api/aggregates/salary") |> GrdevWeb.Endpoint.call([])
      response_body = Jason.decode!(response.resp_body)

      assert %{
               "average" => 2000,
               "maxLowerSalary" => 2000,
               "maxUpperSalary" => 2000,
               "salaries" => [%{"lower" => 2000, "upper" => 2000}]
             } ==
               response_body
    end

    test "#index_advertisement_stats with no advertisements" do
      response = build_conn(:get, "/api/aggregates/advertisements") |> GrdevWeb.Endpoint.call([])
      response_body = Jason.decode!(response.resp_body)

      assert %{
               "numOfAdvertisements" => 0,
               "advertisementsPerCity" => []
             } == response_body
    end

    test "#index_advertisement_stats with default advertisement fixture" do
      JobAdvertisementsTest.job_advertisement_fixture()
      response = build_conn(:get, "/api/aggregates/advertisements") |> GrdevWeb.Endpoint.call([])
      response_body = Jason.decode!(response.resp_body)

      assert %{
               "numOfAdvertisements" => 1,
               "advertisementsPerCity" => [%{"city" => "Thesaloniki", "count" => 1}]
             } == response_body
    end

    test "#index_advertisement_stats with multiple advertisements per city" do
      for city <- ["Athens", "Athens", "Thesaloniki", "Patras"] do
        JobAdvertisementsTest.job_advertisement_fixture(%{city: city})
      end

      response = build_conn(:get, "/api/aggregates/advertisements") |> GrdevWeb.Endpoint.call([])
      response_body = Jason.decode!(response.resp_body)

      assert %{
               "numOfAdvertisements" => 4,
               "advertisementsPerCity" => [
                 %{"city" => "Athens", "count" => 2},
                 %{"city" => "Patras", "count" => 1},
                 %{"city" => "Thesaloniki", "count" => 1}
               ]
             } == response_body
    end
  end

  test "#index_salary_ranges with missing categgory" do
    response =
      build_conn(:get, "/api/aggregates/categorized_salaries") |> GrdevWeb.Endpoint.call([])

    response_body = Jason.decode!(response.resp_body)

    assert %{"error" => "Salary category is missing"} == response_body
  end

  test "#index_salary_ranges with invalid categgory" do
    response =
      build_conn(:get, "/api/aggregates/categorized_salaries?category=asd")
      |> GrdevWeb.Endpoint.call([])

    response_body = Jason.decode!(response.resp_body)

    assert %{"salaries" => []} == response_body
  end

  test "#index_salary_ranges with upper categgory" do
    salaries = [{1000, 1200}, {1300, 1500}, {800, 1000}, {1000, 1100}, {1000, 1050}, {1000, 1020}]

    Enum.each(salaries, fn {lower, upper} ->
      JobAdvertisementsTest.job_advertisement_fixture(%{
        qualifications: %{lower_salary: lower, upper_salary: upper, lower_years_of_experience: 1}
      })
    end)

    response =
      build_conn(:get, "/api/aggregates/categorized_salaries?category=upper")
      |> GrdevWeb.Endpoint.call([])

    response_body = Jason.decode!(response.resp_body)

    expected_result = %{
      "salaries" => [
        %{"range" => %{"lower" => 1000, "upper" => 1020}, "salaries" => [1000, 1020]},
        %{"range" => %{"lower" => 1050, "upper" => 1100}, "salaries" => [1050, 1100]},
        %{"range" => %{"lower" => 1200, "upper" => 1500}, "salaries" => [1200, 1500]}
      ]
    }

    assert expected_result == response_body
  end

  test "#index_salary_ranges with lower categgory" do
    salaries = [{1000, 1200}, {1300, 1500}, {800, 1000}, {1000, 1100}, {1000, 1050}, {1000, 1020}]

    Enum.each(salaries, fn {lower, upper} ->
      JobAdvertisementsTest.job_advertisement_fixture(%{
        qualifications: %{lower_salary: lower, upper_salary: upper, lower_years_of_experience: 1}
      })
    end)

    response =
      build_conn(:get, "/api/aggregates/categorized_salaries?category=lower")
      |> GrdevWeb.Endpoint.call([])

    response_body = Jason.decode!(response.resp_body)

    expected_result = %{
      "salaries" => [
        %{"range" => %{"lower" => 800, "upper" => 800}, "salaries" => [800]},
        %{"range" => %{"lower" => 1000, "upper" => 1000}, "salaries" => [1000]},
        %{"range" => %{"lower" => 1300, "upper" => 1300}, "salaries" => [1300]}
      ]
    }

    assert expected_result == response_body
  end
end
